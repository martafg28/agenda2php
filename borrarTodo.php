<?php
include('cabecera.html');

if(isset($_POST['si'])){
   try{
        $conn = new PDO('sqlite:agenda.db');
        $consulta = "DELETE FROM contactos";
        $sentencia=$conn->prepare($consulta);
        if($sentencia ->execute()){
            echo '<h2>La Agenda se ha borrado</h2>';
        }else{
            echo '<h2>La agenda no se ha podido borrar</h2>';
        }
        
    }catch (PDOException $e){
        
        echo $e ->getMessage();
    }
}
if(isset($_POST['no'])){
    header("Location: index.php");
}

include('pie.html');
?>
