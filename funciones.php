<?php
//crear Base de datos Agenda con la tabla de contactos
function crearBD(){
    // variables
$crear_tabla = 'CREATE TABLE IF NOT EXISTS contactos(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    nombre VARCHAR(40), 
                    apellidos VARCHAR(60), 
                    telefono VARCHAR(10),
                    correo VARCHAR(50)
                    )'; 
                    
//introduciremos unos datos en la tabla contactos   
$datos= array(
              array('nombre'=>"Marta",
                    'apellidos'=>"Lopez",
                    'telefono'=>"976111111",
                    'correo'=>"marta@marta.com"),
                    
              array('nombre'=>"luis",
                    'apellidos'=>"Perez",
                    'telefono'=>"976222222",
                    'correo'=>"luis@luis.com"),
                    
              array('nombre'=>"Jose",
                    'apellidos'=>"Fernandez",
                    'telefono'=>"976333333",
                    'correo'=>"jose@jose.com")
             
        );              
// test de acceso a sqlite
$nombre_fichero = 'agenda.db';
if(file_exists($nombre_fichero)){
    echo "<div class='etiqueta2'>La agenda $nombre_fichero ya existe</div><br>";
    
}else{

    try{
    // crear bases de datos
        $conn=new PDO('sqlite:agenda.db');
        //$conn=new PDO('sqlite::memory:');  --> crea base de datos en memoria no en el sistema de ficheros
        //echo "Funciona<br>";
        $conn->exec($crear_tabla);
        //insertar datos
        //1. preparar sentencia de insercion
        $insertar="INSERT into contactos(nombre,apellidos, telefono, correo)
                    VALUES(:nombre,:apellidos,:telefono,:correo)";
        $sentencia=$conn->prepare($insertar);
        foreach($datos as $contacto){
                $sentencia->execute($contacto);
            
        }
            
        //buscar datos
        $listado='SELECT * FROM contactos';
        $resultado=$conn->query($listado);
        
        echo "<h3>Datos iniciales insertados</h3><br>";
        foreach($resultado as $cont){
            echo 'id: <strong>', $cont['id'], '</strong><br>';
            echo 'nombre: <strong>', $cont['nombre'], '</strong><br>';
            echo 'apellidos: <strong>', $cont['apellidos'], '</strong><br>';
            echo 'telefono: <strong>', $cont['telefono'], '</strong><br>';
            echo 'correo: <strong>', $cont['correo'], '</strong><br><hr>';
                
        }//foreach
    }catch(PDOException $e){
         echo $e->getMessage();
    
    }//del catch

    //cierra conexion
    $conn=null;
    echo "<h2>El fichero '".$nombre_fichero."' ha sido creado con exito.</h2><br>";   //el punto concatena (si pongo comillas dobles)
    }//del else

}//crearBD()


//listar toda la agenda
function listar(){
    try {
    // crear/conectar a bases de datos
    $conn = new PDO('sqlite:agenda.db');
    // select
    $listado = 'SELECT * FROM contactos ORDER BY nombre';
    $resultado=$conn->query($listado);
	if($resultado->fetchColumn()==0){
           echo '<h2>No existe contactos</h2>';
     }               
     else{
     	$listado = 'SELECT * FROM contactos ORDER BY nombre';
    	$resultado=$conn->query($listado); 
       listarResultado($resultado);
        }
     }
     catch(PDOException $e){
        echo $e->getMessage();
    } //fin catch
}//listar



//listar cualquier consulta
function listarResultado($consulta){
    echo "<table class='listado'>
            <tr class='registro'>
                <td><strong>id</strong></td>
                <td><strong>Nombre</strong></td>
                <td><strong>Apellidos</strong></td>
                <td><strong>Telefono</strong></td>                
                <td><strong>correo</strong></td>
            </tr>";  
    foreach($consulta as $cont){
        echo "<tr><td>",$cont['id'],"</td>";
        echo "<td>",$cont['nombre'],"</td>";
        echo "<td>", $cont['apellidos'],"</td>";
        echo "<td>", $cont['telefono'],"</td>";
        echo "<td>", $cont['correo'],"</td>";
        //vamos a editar y enviamos como variable el id del contacto
        echo "<td><a href='editar.php?id=".$cont['id']."'><img src='images/editar.jpg' width='50px' height='50px'  alt='Editar'></a></td>";
        //vamos a borrar y enviamos como parametro el id del contacto
        echo "<td><a href='borrar.php?id=".$cont['id']."'><img src='images/eliminar.jpg' width='50px' height='50px'   alt='Eliminar'></td></tr>";
        }
        echo "</table>";
    
}




function buscar(){
    
    $consulta = "SELECT * FROM contactos WHERE ";
    
    if($_POST['nombre'] != ""){
        $consulta = $consulta . "nombre LIKE '%".ucwords(strtolower($_POST['nombre']))."%'";
        $esta=true;
    }
    if($_POST['apellidos']!= ""){
        if ($and){
            $consulta = $consulta ." AND apellidos LIKE'%".ucwords(strtolower($_POST['apellidos']))."%'";
        }else{
            $consulta = $consulta ." apellidos LIKE'%".ucwords(strtolower($_POST['apellidos']))."%'";
            $esta=true;
        }
    }
    if($_POST['correo'] != ""){
        if ($esta){
            $consulta = $consulta ." AND correo LIKE'%".$_POST['correo']."%'";
        }else{
            $consulta = $consulta ." correo LIKE'%".$_POST['correo']."%'";
            $esta=true;
        }
    }
    if($_POST['telefono'] != ""){
        if ($esta){  
            $consulta = $consulta ." AND telefono ='".$_POST['telefono']."'";
        }else{
            $consulta = $consulta ." telefono ='".$_POST['telefono']."'";
            $and=true;
        }
    }
    
    try{
        $conn = new PDO('sqlite:agenda.db'); //crea conexion y crea la bd si no existe
        $resultado = $conn->query($consulta);
        //comprobamos si existen algun registro
        if($resultado->fetchColumn()==0){
           echo '<h2>No existe contactos</h2>';
        }               
        else{
        	$resultado = $conn->query($consulta);
        	listarResultado($resultado);
        }
     
            
        
    } //fin try
    catch(PDOException $e){
        echo $e->getMessage();
    } //fin catch
   
}// funcion buscar




?>
